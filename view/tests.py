from django.test import TestCase

def add(a, b):
    return a + b

class AddTestCase(TestCase):
    def setUp(self):
        pass
    
    def testInt(self):
        self.assertEquals(add(4, 5), 9)
        self.assertEquals(add(-10, 12), 2)
